unit main;
interface
{ TEdit OnChange event triggered, message shown in TMemo }
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Memo1: TMemo;
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
implementation
{$R *.dfm}

procedure TForm1.Edit1Change(Sender: TObject);
begin
  Memo1.Lines.Append('changes made to TEdit'); 
end;
end.
